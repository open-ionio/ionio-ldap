<?php

class LDAP {
    //error codes
    const LDAP_ERR_OK = 0;
    const LDAP_ERR_EDUPERSON_MISSING = -1;
    const LDAP_ERR_EDUPERSON_PRIMARY_AFF_MISSING = -2;
    const LDAP_ERR_EDUPERSON_PRIMARY_AFF_NOT_IN_AFFS = -3;
    const LDAP_ERR_EDUPERSON_INVALID_AFF = -4;
    const LDAR_ERR_EDUPERSON_MEMBER_AFF_MISSING = -5;
    
	//ldap connection params
	protected $_ldapURL;
	protected $_ldapBaseDN;
	protected $_method = 'bind'; //bind, anonymous+search, searchonly

	//filter params
	protected $_uid;
	protected $_registryno;
	protected $_dept;
	protected $_filters;
	protected $_filter;

	//Transient
	protected $_bindDN = null;
	protected $_ds = null; //ldap handle
	protected $_bound = false;

	//public
	public $error = '';

	public function __construct($ldapURL='ldap://127.0.0.1',$baseDN='cn=config') {
		$this->_ldapURL = $ldapURL;
		$this->_ldapBaseDN = $baseDN;
	}

	public function addFilter($filter) {
		$this->_filters[] = $filter;
	}

	private function buildFilter() {
		$this->_filter = '';
		$count = 0;
		foreach($this->filters as $filter) {
			if ( $count == 0 ) //first filter
				$this->_filter = $filter;
			else //second+ filter
				$this->_filter = '&('.$this->_filter.')('.$filter.')';
		}
	}

	public function setUid($uid) {
		$this->_uid = $uid;
		$this->buildBindDN();
	}

	public function setDept($dept) {
		$this->_dept = $dept;
		$this->bildBindDN();
	}

	public function setRegistryNo($registryno) {
		$this->registryno = $registryno;
		$this->buildBindDN();
	}

	//directly set bind DN, bypassing uid/dept calculation
	public function setBindDN($bDN) {
		$this->_bindDN = $bDN;
	}
	
	//use this to go from user bind to anonymous bind
	public function clearBindDN() {
		$this->_bindDN = null;
	}
	
	//In its current incarnation, this requires _uid and optionally _dept
	protected function buildBindDN() {
		$this->_bindDN='uid='.$this->_uid.','.
			($this->_dept ? 'ou='.$this->_dept.',':'').
			$this->_ldapBaseDN;
		//echo 'BUIILD: '.$this->_bindDN;
	}

	public function setup() {
		if ( $this->_ds !== null ) return $this->_ds; //already setup
		if ( $this->_bound ) return false; //already setup and bound - this is an error

		if ( !function_exists('ldap_connect') ) {
		    die('YOU ARE MISSING THE PHP LDAP EXTENSION');
		}
		$this->_ds = ldap_connect($this->_ldapURL);
		if ( $this->_ds ) {
			ldap_set_option($this->_ds, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($this->_ds, LDAP_OPT_REFERRALS, 0);
			return $this->_ds;
		}
		$this->_ds = null;
		return false;
	}

	public function isSetup() { return $this->_ds !== null; }
	public function isBound() { return $this->_bound; }

	public function bindAnonymous($allowRebind=false) {
	    if ( $this->_bindDN !== null ) {
	        $this->error = "You must clear the bind DN before binding anonymously";
	        return false;
	    }
	    
	    if ( !$this->setup() ) return false;
	    if ( !$allowRebind && $this->_bound ) return false;
        $r = @ldap_bind($this->_ds); //anonymous bind
        if ( !$r ) {
            $this->error = ldap_error($this->_ds);
            $this->_bound = false;
            $this->close();
            return false;
        }
        $this->_bound = true;
        return true;
	}
	
	public function bind($password,$allowRebind=false) {
		if ( !$this->setup() ) return false;
		if ( !$allowRebind && $this->_bound ) return false;
		if (empty($password)) {
		    $this->error = 'Κενός κωδικός πρόσβασης';
		    return false;
		}
		else {
			//echo 'DS:'.$this->_ds.' bindDN: '.$this->bindDN;
			$r = @ldap_bind($this->_ds,$this->_bindDN,$password);
		}
		if ( !$r ) {
			$this->error = ldap_error($this->_ds);
			$this->_bound = false;
			$this->close();
			return false;
		}
		$this->_bound = true;
		return true;
	}

	public function close() {
		@ldap_close($this->_ds);
		$this->_ds = null;
	}

	public function getData($uid,$fields=array('dn')) {
		if ( !$this->_bound ) return false;
		$sr = ldap_search($this->_ds,$this->_ldapBaseDN,"(uid=$uid)",$fields);
		if ( ldap_count_entries($this->_ds, $sr) != 1 ) {
			$this->error = ldap_error($this->_ds);
			$this->close();
    			return false;
		}

		$info = ldap_get_entries($this->_ds, $sr);
		if ( !$info ) {
			$this->error = ldap_error($this->_ds);
			$this->close();
    			return false;
		}
	  	return $info[0];
	}

	public static function implode($delim,$data) {
		if (isset($data['count'])) unset($data['count']);
		return implode($delim,$data);
	}

	public static function to_array($data) {
		if ( isset($data['count'])) unset($data['count']);
		return $data;
	}
	
	public static function cndept_to_dept($cndept) {
	    switch($cndept) {
	        case 'history' : return 'Ιστορίας'; break;
	        case 'dflti' : return 'Ξένων Γλωσσών, Μετάφρασης & Διερμηνείας'; break;
	        case 'tab' : return 'Αρχειονομίας, Βιβλιοθηκονομίας & Μουσειολογίας'; break;
	        case 'di' : return 'Πληροφορικής'; break;
	        case 'avarts' : return 'Τεχνών Ήχου & Εικόνας'; break;
	        case 'music' : return 'Μουσικών Σπουδών'; break;
	        case 'tourism' : return 'Τουρισμού'; break;
	        case 'regdev' : return 'Περιφερειακής Ανάπτυξης'; break;
	        case 'dmc' : return 'Ψηφιακών Μέσων και Επικοινωνίας'; break;
	        case 'envi' : return 'Περιβάλλοντος'; break;
	        case 'food' : return 'Επιστήμης και Τεχνολογίας Τροφίμων'; break;
	        case 'timo' : return 'Εθνομουσικολογίας'; break;
	        case 'admin' : return 'Διοίκηση'; break;
	        default : return $cndept;
	    }
	}
	
	public static function affiliationText(string $affiliation) {
	    switch($affiliation) {
	        case 'student': return 'Φοιτητής'; break;
	        case 'faculty': return 'Διδακτικό προσωπικό'; break;
	        case 'staff': return 'Προσωπικό'; break;
	        case 'alum': return 'Απόφοιτος'; break;
	        case 'member': return 'Μέλος'; break;
	        case 'affiliate': return 'Συνεργάτης'; break;
	        case 'employee': return 'Υπάλληλος'; break;
	        case 'library-walk-in': return 'Χρήστης περιορισμένης πρόσβασης'; break;
	        default: return 'Άγνωστο';
	    }
	}

	
	/**
	 * @param array $userdata Array of user record in LDAP
	 * @param string $aff Affiliation we're looking for
	 */
	public static function eduPersonAffiliationContains(array $userdata,string $aff,$check=true) {
	    $x = static::eduPersonCheckRecord($userdata);
	    if ( $x !== self::LDAP_ERR_OK )
	        return false;
	   
	    return $aff == $userdata['edupersonprimaryaffiliation'] || in_array($aff,$userdata['edupersonaffiliation']);
	}
	
	public static function eduPersonCheckRecord(array $userdata) {
	    if ( !in_array('eduPerson',$userdata['objectclass']) ) {
	        return self::LDAP_ERR_EDUPERSON_MISSING;
	    }
	    
	    if ( !array_key_exists('edupersonprimaryaffiliation',$userdata) )
	    {
	        return self::LDAP_ERR_EDUPERSON_PRIMARY_AFF_MISSING;
	    }

	    /*
	    if ( !in_array($userdata['edupersonprimaryaffiliation'],$userdata['edupersonaffiliation']) )
	    {
	        return self::LDAP_ERR_EDUPERSON_PRIMARY_AFF_NOT_IN_AFFS;
	    }
	    */
	    
        if ( !array_key_exists('edupersonprimaryaffiliation',$userdata) )
        {
            return self::LDAP_ERR_EDUPERSON_PRIMARY_AFF_MISSING;
        }
	    
        /*
        foreach($userdata['edupersonaffiliation'] as $a) {
            if ( !in_array($a,['staff','faculty','student','alum','member','affiliate','employee','library-walk-in']) ) {
                return self::LDAP_ERR_EDUPERSON_INVALID_AFF;
            }
        }
        */
        
        /*
        if ( in_array('faculty',$userdata['edupersonaffiliation']) || 
            in_array('staff',$userdata['edupersonaffiliation']) ||
            in_array('employee',$userdata['edupersonaffiliation']) ||
            in_array('student',$userdata['edupersonaffiliation']) ) 
        {
            if ( !in_array('member',$userdata['edupersonaffiliation']) ) {
                return self::LDAR_ERR_EDUPERSON_MEMBER_AFF_MISSING;
            }
        }
        */

        return self::LDAP_ERR_OK;
	}
	
	//add deptou's from eduPersonOrgUnitDN
	public static function getAllDepartments(array $userdata) {
	    $deptou = [ str_replace('ou=','',explode(',',$userdata['dn'])[1]) ];
	    if ( array_key_exists('edupersonorgunitdn',$userdata) ) {
	        foreach(static::to_array($userdata['edupersonorgunitdn']) as $orgdn) {
	            $ou = str_replace('ou=','',explode(',',$orgdn)[0]);
	            if ( !in_array($ou,$deptou) ) {
	                $deptou[] = $ou;
	            }
	        }
	    }
	    return $deptou;
	}
	
	public static function ldapErrorText($err) {
	    switch($err) {
	        case self::LDAP_ERR_OK: return "Κανένα σφάλμα"; break;
	        case self::LDAP_ERR_EDUPERSON_MISSING : return "Δεν υπάρχει objectclass eduperson"; break;
	        case self::LDAP_ERR_EDUPERSON_PRIMARY_AFF_MISSING : return "Δεν υπάρχει primary affiliation";  break;
	        case self::LDAP_ERR_EDUPERSON_PRIMARY_AFF_NOT_IN_AFFS : return "Το primary affiliation δεν είναι στα affiliations";  break;
	        case self::LDAP_ERR_EDUPERSON_INVALID_AFF : return "Το affiliation δεν είναι στις επιτρεπόμενες τιμές";  break;
	        case self::LDAR_ERR_EDUPERSON_MEMBER_AFF_MISSING: return "Το affiliation 'member' πρέπει να υπάρχει στα affiliations με βάση τις άλλες τιμές τους";  break;
	        default: return 'Άγνωστο σφάλμα';
	    }
	}
	
}














